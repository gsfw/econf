#ifndef ZK_CLIENT_H
#define ZK_CLIENT_H


#include <map>
#include <set>
#include <string>
#include <vector>
#include <pthread.h>
#include <zookeeper/zookeeper.h>


typedef std::vector<std::string> s_vector;
typedef struct _zhandle zhandle_t;

std::string toZkEventType(int type);
std::string toZkState(int state);
std::string zkPath(const std::string& parent, const std::string& child);

class ZKClient{
public:
	enum{
		STATUS_OK = 0,
		STATUS_NOT_INIT = -10,
	};

	enum{
		DEFAULT_TIMEOUT = 30000,
	};

	ZKClient();
	~ZKClient();

	int init(const std::string& zkurl, 
		int timout_ms = DEFAULT_TIMEOUT);

	int clear();

	int getNodeData(const std::string& path, std::string& data);
	//no watch
	int getChildren(const std::string& path, s_vector& us);
private:
	static void watcherFnG(zhandle_t * zh, int type, int state,
        const char *path, void *watcherCtx);
	int doInit();
	int reinit();
	int connectingFinishSignal();
	int waitConnectingFinish();
#if 0
	int clearPathWatchers();
	int addChildrenWatcher(const std::string& path);
	int getChildren(const std::string& path, urls& us);
	bool getChildrenFromCache(const std::string& path, urls& us);
	bool addChildrenCacheIfNoExist(const std::string& path, 
		const urls&us, urls& oldus);
	int broadcastChildrenChange(const std::string& path, const urls& us); 

#endif	

	std::string m_zkurl;
	int m_timeout_ms;
	zhandle_t* m_zkhandle;
	pthread_mutex_t m_cs;
	pthread_cond_t m_cond;
	bool m_waiting;
};



#endif
