#include "zk_client.h"
#include <sstream>
#include <iostream>
#include <algorithm>
//#include "base/ef_thread.h"


	using namespace std;

	typedef s_vector urls;

	std::string toZkEventType(int type){
		if(ZOO_CHILD_EVENT == type)
			return "ZOO_CHILD_EVENT";

		if(ZOO_CHANGED_EVENT == type)
			return "ZOO_CHANGED_EVENT";

		if(ZOO_CREATED_EVENT == type)
			return "ZOO_CREATED_EVENT";

		if(ZOO_DELETED_EVENT == type)
			return "ZOO_DELETED_EVENT";

		if(ZOO_SESSION_EVENT == type)
			return "ZOO_SESSION_EVENT";

		stringstream os;
		os << type;
		return os.str();
	}

	std::string toZkState(int state){
		if(ZOO_EXPIRED_SESSION_STATE == state)
			return "ZOO_EXPIRED_SESSION_STATE";

		if(ZOO_AUTH_FAILED_STATE == state)
			return "ZOO_AUTH_FAILED_STATE";

		if(ZOO_CONNECTING_STATE == state)
			return "ZOO_CONNECTING_STATE";

		if(ZOO_ASSOCIATING_STATE == state)
			return "ZOO_ASSOCIATING_STATE";

		if(ZOO_EXPIRED_SESSION_STATE == state)
			return "ZOO_EXPIRED_SESSION_STATE";

		if(ZOO_CONNECTED_STATE == state)
			return "ZOO_CONNECTED_STATE";

		stringstream os;
		os << state;
		return os.str();
	}
	std::string zkPath(const std::string& parent, const std::string& child){
		std::string path = parent + "/" + child;
		std::string old = "//";
		while(1){   
			string::size_type   pos(0);
			if((pos=path.find(old)) != std::string::npos)
				path.replace(pos,old.size(),"/");
			else
				break;   
		}
		return path;
	}

	ZKClient::ZKClient()
		:m_timeout_ms(DEFAULT_TIMEOUT),
		m_zkhandle(NULL), m_waiting(false){
			pthread_mutex_init(&m_cs, NULL);
			pthread_cond_init(&m_cond, NULL);	
	}

	ZKClient::~ZKClient(){
		clear();
	}

	int ZKClient::getNodeData(const string& path, string& data){
		int ret = 0;
		struct Stat s;
		data.resize(1024 * 1000);
		char* buf = (char*)data.data();
		int buflen = data.size();
		ret = zoo_get(m_zkhandle, path.data(), 0, buf, &buflen, &s);
		if(!ret){
			if (buflen < 0){
				buflen = 0;
			}
			
			data.resize(buflen);
			return 0;
		}
	
		return ret;
	}

	int ZKClient::init(const string& zkurl,
		int timout_ms){
		m_zkurl = zkurl;
		m_timeout_ms = timout_ms;
		int ret = doInit();
		if(0 == ret){
			waitConnectingFinish();
			if(zoo_state(m_zkhandle)!=ZOO_CONNECTED_STATE){
				zookeeper_close(m_zkhandle);
				m_zkhandle = NULL;
				ret = -1;
			}
		}
		return ret;
	}

	int ZKClient::reinit(){
		if(m_zkhandle){
			zookeeper_close(m_zkhandle);
			m_zkhandle = NULL;
		}
		return doInit();
	}
	
	int ZKClient::doInit(){
		//m_zkhandle = zookeeper_init(m_zkurl.data(), ZKClient::watcherFnG, m_timeout_ms, NULL, this, 0);
		m_zkhandle = zookeeper_init(m_zkurl.data(), ZKClient::watcherFnG, m_timeout_ms, NULL, this, 0);
		return m_zkhandle == NULL?-1:0;
	}

	void ZKClient::watcherFnG(zhandle_t * zh, int type, int state,
                const char *path, void *watcherCtx){
                ZKClient* c = (ZKClient*)watcherCtx;
                if(type == ZOO_SESSION_EVENT){
                        if(state == ZOO_CONNECTED_STATE){
                                c->connectingFinishSignal();
                        }
                        return;
                }
        }
	
	int ZKClient::connectingFinishSignal(){
		if(m_waiting){
			pthread_mutex_lock(&m_cs);
			pthread_cond_signal(&m_cond);
			pthread_mutex_unlock(&m_cs);
		}
		return 0;
	}

	int ZKClient::waitConnectingFinish(){
		m_waiting = true;
		pthread_mutex_lock(&m_cs);
		pthread_cond_wait(&m_cond, &m_cs);
		pthread_mutex_unlock(&m_cs);
		m_waiting = false;
		return 0;
	}

	int ZKClient::clear(){
		//clear watcher is job of who create it
		//clearPathWatchers();
		if(m_zkhandle){
			zookeeper_close(m_zkhandle);
			m_zkhandle = NULL;
		}
		pthread_mutex_destroy(&m_cs);
		pthread_cond_destroy(&m_cond);
		return 0;
	}

	int ZKClient::getChildren(const string& path, s_vector& us){
		int ret = 0;
		struct String_vector s_v;
		ret = zoo_get_children(m_zkhandle, path.data(), 
			0, &s_v);

		if (ret){
			return ret;
		}

		for(int i = 0; i < s_v.count; ++i){
			us.push_back(s_v.data[i]);
		}	
		deallocate_String_vector(&s_v);	
		return 0;		
	}
