#!/bin/bash

yum -y install cmake gcc-c++
unzip jsoncpp-master.zip && cd jsoncpp-master && cmake . && make && make install && cd -
tar -xvzf zookeeper-3.4.8.tar.gz && cd zookeeper-3.4.8/src/c && ./configure && make && make install && cd -
cmake . && make && make install

