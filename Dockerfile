FROM 192.168.250.75:5000/centos:7
RUN yum -y install cmake gcc-c++
RUN yum -y install unzip
RUN yum -y install make
RUN mkdir /build
WORKDIR /build
COPY jsoncpp-master.zip /build
RUN unzip jsoncpp-master.zip && cd jsoncpp-master && cmake . && make && make install && rm -rf /build/jsoncpp-master
COPY zookeeper-3.4.8.tar.gz /build
RUN tar -xvzf zookeeper-3.4.8.tar.gz && cd zookeeper-3.4.8/src/c && ./configure && make && make install && rm -rf /build/zookeeper-3.4.8
COPY *.cpp /build/
COPY *.h /build/
COPY CMakeLists.txt /build/
RUN cmake . && make && make install
RUN jr -v
